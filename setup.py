from distutils.core import setup
from Cython.Build import cythonize

setup(
        name='TestCython',
        ext_modules=cythonize("src/*.pyx"),
)
