from module import test
import timeit
import csv
import os
import numpy
import datetime
import pandas
from matplotlib import pyplot

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

rsi_ema = lambda array, n: pandas.ewma(array, span=n, adjust=False)
rsi_sma = lambda array, n: pandas.rolling_mean(array, window=n)

name = 'HK2628'
name = 'HK2388'


def run_rsi_array(close_array, n, kernel=rsi_sma):
    delta = numpy.ediff1d(close_array, to_begin=numpy.nan)
    # print len(delta)
    delta_up = delta.copy()
    delta_down = delta.copy()
    delta_up[delta_up < 0] = 0
    delta_down[delta_down > 0] = 0

    # average_up = pandas.ewma(delta_up, span=n, adjust=False)
    # average_down = pandas.ewma(delta_down, span=n, adjust=False)
    average_up = kernel(delta_up, n)
    average_down = kernel(delta_down, n)

    # print delta_up[:20]
    # print average_up[:20]
    #
    # print len(average_up)
    # print len(average_down)

    return average_up / (average_up - average_down)


def run_stochastic_oscillator(close_array, high_array, low_array, n=14, n_d=3):
    running_lowest_low = pandas.rolling_apply(low_array, n, numpy.min)
    running_highest_high = pandas.rolling_apply(high_array, n, numpy.max)
    percent_k = (close_array - running_lowest_low) / (running_highest_high - running_lowest_low)
    # print running_lowest_low, running_highest_high, percent_k
    percent_d = pandas.rolling_mean(percent_k, window=n_d)
    percent_d2 = pandas.rolling_mean(percent_d, window=n_d)
    return percent_k, percent_d, percent_d2


def run_macd(close_array, a=12, b=26, c=9):
    macd_1 = pandas.ewma(close_array, span=a)
    macd_2 = pandas.ewma(close_array, span=b)
    macd_3 = macd_1 - macd_2
    macd_4 = pandas.ewma(macd_3, span=c)
    return macd_3, macd_4


def run_ema_list(list, n):
    alpha = 2.0 / (n + 1)
    new_list = []
    last_value = list[0]
    new_list.append(last_value)
    for i in range(1, len(list)):
        last_value += alpha * (list[i] - last_value)
        new_list.append(last_value)
    return new_list


def run_rsi_list(list, n):
    diff_list = []
    for i in range(1, len(list)):
        diff_list.append(list[i] - list[i - 1])
    # run_ema(list, )
    pass


def get_csv_file(file_name):
    with open(os.path.join(BASE_DIR, os.pardir, 'tests', file_name)) as f:
        csv_reader = csv.DictReader(f)
        result = []
        for line in csv_reader:
            result.append((
                # datetime.datetime.strptime(line['Date'], '%Y-%m-%d').date(),
                datetime.datetime.strptime(line['Date'], '%Y-%m-%d %H:%M:%S.%f'),
                round(float(line['Open']), 3),
                round(float(line['Adj Close']), 3),
                round(float(line['High']), 3),
                round(float(line['Low']), 3),
            ))
        # return list(reversed(result))
        return list(result)


class Test1(object):
    def __init__(self):
        # self.date_list = list(datetime.datetime.strptime(d, '%m/%d/%Y').date() for d in ['01/02/2015','01/05/2015','01/06/2015','01/07/2015','01/08/2015','01/09/2015','01/12/2015','01/13/2015','01/14/2015','01/15/2015','01/16/2015','01/20/2015','01/21/2015','01/22/2015','01/23/2015','01/26/2015','01/27/2015','01/28/2015','01/29/2015','01/30/2015','02/02/2015','02/03/2015','02/04/2015','02/05/2015','02/06/2015','02/09/2015','02/10/2015','02/11/2015','02/12/2015','02/13/2015','02/17/2015','02/18/2015','02/19/2015','02/20/2015','02/23/2015','02/24/2015','02/25/2015','02/26/2015','02/27/2015','03/02/2015','03/03/2015','03/04/2015','03/05/2015','03/06/2015','03/09/2015','03/10/2015','03/11/2015','03/12/2015','03/13/2015','03/16/2015','03/17/2015','03/18/2015','03/19/2015','03/20/2015','03/23/2015','03/24/2015','03/25/2015','03/26/2015','03/27/2015','03/30/2015','03/31/2015','04/01/2015','04/02/2015','04/06/2015','04/07/2015','04/08/2015','04/09/2015','04/10/2015','04/13/2015','04/14/2015','04/15/2015','04/16/2015','04/17/2015','04/20/2015','04/21/2015','04/22/2015','04/23/2015','04/24/2015','04/27/2015','04/28/2015','04/29/2015','04/30/2015','05/01/2015','05/04/2015','05/05/2015','05/06/2015','05/07/2015','05/08/2015','05/11/2015','05/12/2015','05/13/2015','05/14/2015','05/15/2015','05/18/2015','05/19/2015','05/20/2015','05/21/2015','05/22/2015','05/26/2015','05/27/2015','05/28/2015','05/29/2015','06/01/2015','06/02/2015','06/03/2015','06/04/2015','06/05/2015','06/08/2015','06/09/2015','06/10/2015','06/11/2015','06/12/2015','06/15/2015','06/16/2015','06/17/2015','06/18/2015','06/19/2015','06/22/2015','06/23/2015','06/24/2015','06/25/2015','06/26/2015','06/29/2015','06/30/2015','07/01/2015','07/02/2015','07/06/2015','07/07/2015','07/08/2015','07/09/2015','07/10/2015','07/13/2015','07/14/2015','07/15/2015','07/16/2015','07/17/2015','07/20/2015','07/21/2015','07/22/2015','07/23/2015','07/24/2015','07/27/2015','07/28/2015','07/29/2015','07/30/2015','07/31/2015','08/03/2015','08/04/2015','08/05/2015','08/06/2015','08/07/2015','08/10/2015','08/11/2015','08/12/2015','08/13/2015','08/14/2015','08/17/2015','08/18/2015','08/19/2015','08/20/2015','08/21/2015','08/24/2015','08/25/2015','08/26/2015','08/27/2015','08/28/2015','08/31/2015','09/01/2015','09/02/2015','09/03/2015','09/04/2015','09/08/2015','09/09/2015','09/10/2015','09/11/2015','09/14/2015','09/15/2015','09/16/2015','09/17/2015','09/18/2015','09/21/2015','09/22/2015','09/23/2015','09/24/2015','09/25/2015','09/28/2015','09/29/2015','09/30/2015','10/01/2015','10/02/2015','10/05/2015','10/06/2015','10/07/2015','10/08/2015','10/09/2015','10/12/2015','10/13/2015','10/14/2015','10/15/2015','10/16/2015','10/19/2015','10/20/2015','10/21/2015','10/22/2015','10/23/2015','10/26/2015','10/27/2015','10/28/2015','10/29/2015','10/30/2015','11/02/2015','11/03/2015','11/04/2015','11/05/2015','11/06/2015','11/09/2015','11/10/2015','11/11/2015','11/12/2015','11/13/2015','11/16/2015','11/17/2015','11/18/2015','11/19/2015','11/20/2015','11/23/2015','11/24/2015','11/25/2015','11/27/2015','11/30/2015','12/01/2015','12/02/2015','12/03/2015','12/04/2015','12/07/2015','12/08/2015','12/09/2015','12/10/2015','12/11/2015','12/14/2015','12/15/2015','12/16/2015','12/17/2015','12/18/2015','12/21/2015','12/22/2015','12/23/2015','12/24/2015','12/28/2015','12/29/2015','12/30/2015','12/31/2015','01/04/2016','01/05/2016','01/06/2016','01/07/2016','01/08/2016','01/11/2016','01/12/2016','01/13/2016','01/14/2016','01/15/2016','01/19/2016','01/20/2016','01/21/2016','01/22/2016','01/25/2016','01/26/2016','01/27/2016','01/28/2016','01/29/2016','02/01/2016','02/02/2016','02/03/2016','02/04/2016','02/05/2016','02/08/2016','02/09/2016','02/10/2016','02/11/2016','02/12/2016','02/16/2016','02/17/2016','02/18/2016','02/19/2016','02/22/2016'])
        # self.close_list = [106.918237,103.906178,103.91596,105.373089,109.421762,109.539117,106.84,107.788603,107.37787,104.463604,103.651911,106.321692,107.133385,109.920514,110.487721,110.605069,106.732426,112.766317,116.277127,114.575513,116.013079,116.032642,116.922564,117.757104,116.765484,117.541107,119.799242,122.607191,124.158437,124.767156,125.503506,126.377307,126.112217,127.14311,130.579411,129.764515,126.446026,128.046365,126.122045,126.740569,127.005659,126.200576,124.109352,124.295889,124.826061,122.24393,120.01524,122.185017,121.340668,122.675917,124.727883,126.131857,125.17951,123.608631,124.894787,124.384254,121.134491,121.97884,121.00686,124.070079,122.165384,121.98866,123.039186,125.032239,123.71663,123.314088,124.256616,124.786789,124.541339,124.001353,124.472613,123.873714,122.47956,125.277689,124.600252,126.279121,127.310014,127.908913,130.235775,128.183816,126.298762,122.872281,126.603117,126.357667,123.510453,122.73483,123.493969,125.820696,124.539021,124.095369,124.233394,127.131938,126.954483,128.354461,128.236158,128.226289,129.537539,130.67132,127.79249,130.178369,129.92204,128.443189,128.699517,128.127708,128.285441,127.536161,126.836165,125.998158,125.623511,127.062933,126.777013,125.377036,125.13056,125.800973,125.505208,126.077025,124.815072,125.810835,125.23901,126.303785,125.702385,124.962959,122.774258,123.66157,124.815072,124.657332,124.223533,123.917906,120.841892,118.37714,121.541881,123.888331,123.839032,125.031972,126.69814,127.79249,130.20796,128.906563,123.454532,123.39538,122.744682,121.03907,121.64047,121.255969,120.644715,119.589801,116.770124,113.023697,113.772984,114.020572,114.406813,118.566345,112.396376,114.129513,114.040384,114.842576,116.031017,115.377373,113.901733,111.564474,104.740869,102.126309,102.74033,108.632999,111.831869,112.198306,111.673415,106.681981,111.257456,109.306446,108.21704,111.227747,109.088565,111.485243,113.109439,114.198838,115.159492,115.288244,112.822233,112.356761,114.099803,112.307247,113.21838,113.891827,113.604621,111.356499,108.009065,109.237121,108.524058,109.316344,109.712491,110.237383,109.712491,108.444827,111.039583,110.524589,110.712761,109.147984,110.782086,109.969988,110.653341,112.673677,112.663779,114.387009,117.932513,114.169128,113.446167,118.120677,119.368538,118.348464,120.012276,121.388881,120.824373,120.267398,120.406641,119.919288,116.139793,115.483359,115.095465,111.733702,113.563775,113.076422,116.656991,118.138948,118.656145,117.114508,118.238406,117.392995,117.174181,117.661542,116.706717,115.65244,114.578267,118.387598,117.641646,117.591921,114.996006,115.543033,112.569172,111.872953,109.893688,110.739099,108.391842,105.457759,106.750746,106.651287,108.023837,107.446965,106.243496,108.153132,106.740798,104.691918,104.781429,102.155677,100.156523,95.92946,96.43671,97.998236,99.420519,96.864389,98.982891,96.60579,96.138333,96.267629,95.780276,100.872638,98.903329,99.450356,92.915814,93.582196,96.814656,95.909571,93.970098,95.830001,96.599998,94.019997,95.010002,94.989998,94.269997,93.699997,93.989998,96.639999,98.120003,96.260002,96.040001,96.879997]

        # self.__csv = get_csv_file('AAPL.csv')
        self.__csv = get_csv_file(name + '.csv')
        self.date_list, self.open_list, self.close_list, self.high_list, self.low_list = zip(*self.__csv)

        # self.date_list = list(datetime.datetime.strptime(a[0], '%Y-%m-%d').date() for a in self.__csv)
        # # self.close_list = list(round(float(a[1]), 3) for a in self.__csv)
        # self.open_list = list(round(float(a[1]), 4) for a in self.__csv)
        # self.close_list = list(round(float(a[6]), 4) for a in self.__csv)
        # self.high_list = list(round(float(a[2]), 4) for a in self.__csv)
        # self.low_list = list(round(float(a[3]), 4) for a in self.__csv)
        # split = -429
        # self.close_list = list(a / 7.0 for a in self.close_list[:split]) + self.close_list[split:]

        self.original_date_array = numpy.array(self.date_list)
        self.data_frame = pandas.DataFrame({
            'Close': pandas.Series(numpy.array(self.close_list), self.original_date_array),
            'High': pandas.Series(numpy.array(self.high_list), self.original_date_array),
            'Low': pandas.Series(numpy.array(self.low_list), self.original_date_array),
        })

        # self.data_frame = self.data_frame.resample('2s', closed='left', how='last', fill_method='pad')
        self.data_frame = self.data_frame.resample('1min', closed='left', how='last', fill_method='pad')
        # print self.data_frame['Close']
        # print self.data_frame['Close'].index.values

        self.date_array = self.data_frame['Close'].index.values
        self.close_array = self.data_frame['Close']
        self.high_array = self.data_frame['High']
        self.low_array = self.data_frame['Low']

    def run(self):
        # return pandas.ewma(self.close_array, span=14, adjust=False)
        return run_rsi_array(self.close_array, 14)


def measure_time(*args, **kwargs):
    number = 1000
    total_time = timeit.timeit(*args, number=number, **kwargs)
    return total_time / number


the_test = Test1()

# print the_test.date_array[:100]
print(measure_time('the_test.run()', 'from __main__ import the_test'))


def main():
    # print(test(9))

    # price_ewma = the_test.run()
    # print run_ema_list(the_test.close_list, 5)[:20]
    # print price_ewma[:20]
    # price_rsi = run_rsi_array(the_test.close_array, 14)
    price_rsi_sma = run_rsi_array(the_test.close_array, 14)
    price_so_k, price_so_d, price_so_d2 = run_stochastic_oscillator(the_test.close_array,
                                                                    the_test.high_array,
                                                                    the_test.low_array, 14)
    macd_1, macd_2 = run_macd(the_test.close_array)

    # select_data = lambda x: x[-300:]
    select_data = lambda x: x
    fig = pyplot.figure(figsize=[30, 16])
    ax1 = pyplot.subplot2grid([5, 1], [0, 0], rowspan=2)
    ax2 = pyplot.subplot2grid([5, 1], [2, 0])
    ax3 = pyplot.subplot2grid([5, 1], [3, 0])
    ax4 = pyplot.subplot2grid([5, 1], [4, 0])
    # fig, (ax1, ax2) = pyplot.subplots(2, 1, sharex=True, )

    for ax in (ax1, ax2, ax3, ax4):
        ax.grid(True)
        gridlines = ax.get_xgridlines() + ax.get_ygridlines()
        for line in gridlines:
            line.set_linestyle('-')

    for ax in (ax2,):
        # ax.set_ylim([0, 1])
        # ax.set_yticks(numpy.arange(0, 1, 0.1))
        # ax.set_yticks(numpy.arange(0, 1, 0.05), minor=True)
        ax.grid(which='major', alpha=0.5)
        ax.grid(which='minor', alpha=0.3)

        # ax.set_yticks(numpy.arange(0, 1, 0.1))
        # ax.set_yticks(numpy.arange(0, 1, 0.05), minor=True)

        # print the_test.date_array, len(the_test.date_array)
    # print the_test.close_array, len(the_test.close_array)

    # print(price_rsi_sma[:1000])

    date_array = select_data(the_test.date_array)
    price_handle, = ax1.plot(date_array, select_data(the_test.close_array))
    ax1.legend([price_handle], [name + ' Price'])
    # ax2.plot(date_array, select_data(price_rsi))
    rsi_handle, = ax2.plot(date_array, select_data(price_rsi_sma))
    so_k_handle, = ax3.plot(date_array, select_data(price_so_k))
    so_d_handle, = ax3.plot(date_array, select_data(price_so_d))
    so_d2_handle, = ax3.plot(date_array, select_data(price_so_d2))
    macd_1_handle, = ax4.plot(date_array, select_data(macd_1))
    macd_2_handle, = ax4.plot(date_array, select_data(macd_2))
    ax2.legend(*zip(
            (rsi_handle, 'RSI (14)'),
    ), mode='expand', loc='upper left', ncol=4, bbox_to_anchor=(0., 1.02, 1., .102))
    ax3.legend(*zip(
            (so_k_handle, 'Stochastic Oscillator (Fast %K)'),
            (so_d_handle, 'Stochastic Oscillator (Fast %D)'),
            (so_d2_handle, 'Stochastic Oscillator (Slow %D)'),
    ), mode='expand', loc='upper left', ncol=4, bbox_to_anchor=(0., 1.02, 1., .102))
    ax4.legend(*zip(
            (macd_1_handle, 'MACD Line'),
            (macd_2_handle, 'MACD Signal Line'),
    ), mode='expand', loc='upper left', ncol=4, bbox_to_anchor=(0., 1.02, 1., .102))
    fig.savefig(name + '.png')

    # print the_test.date_array[numpy.where(price_rsi_sma < 0.2)][:-5]
    # print price_rsi[price_rsi < 0.2]

    # close_list = list(a[1] for a in result)

    # print pandas.ewma(price, span=5, adjust=False)

    # print(measure_time('the_test.run()', 'from __main__ import the_test'))

    # close_list = [1] + [0] * 50

    # print close_list[:20]
    # print run_ema(close_list, 4)[:20]


if __name__ == '__main__':
    main()
